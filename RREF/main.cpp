#include <iostream>
#include <vector>
#include "../gauss.h"
#include "../rational.h"

template <typename T>
int __main(bool detailed, size_t n, size_t m) {
    Matrix<T> matrix(n, Row<T>(m));

    for (Row<T> &row : matrix)
        for (T &elem : row)
            std::cin >> elem;

    for (RowOperation<T>* op : RREF_ops(matrix))
    {
        if (detailed) op->show(std::cout << '\n' << matrix << '\n');
        (*op)(matrix);
    }

    std::cout << '\n' << matrix;

    return 0;
}

int main()
{
    bool detailed, fractions;
    int n, m;
    std::cin >> detailed >> fractions >> n >> m;

    if (fractions)
        return __main<Rational>(detailed, n , m);
    else
        return __main<double>(detailed, n , m);
}