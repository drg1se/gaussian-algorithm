#include <iostream>
#include <vector>
#include "../gauss.h"
#include "../rational.h"

template <typename T>
int __main(bool detailed, T, size_t n, size_t m) {
    Matrix<T> matrix(n, Row<T>(m));

    for (Row<T> &row : matrix)
        for (T &elem : row)
            std::cin >> elem;

    std::vector<RowOperation<T> *> ops = RREF_ops(matrix);

    // creating the identity matrix
    Matrix<T> identity(n, Row<T>(m, 0));
    for (size_t i = 0; i < n && i < m; ++i)
        identity[i][i] = 1;

    for (RowOperation<T> *op : ops)
    {
        if (detailed) op->show(std::cout << '\n' << identity << '\n');
        (*op)(identity);
    }

    std::cout << '\n' << identity;

    return 0;
}

int main()
{
    bool detailed, fractions;
    int n, m;
    std::cin >> detailed >> fractions >> n >> m;

    if (fractions)
        return __main(detailed, Rational(), n , m);
    else
        return __main(detailed, (double) 0, n , m);
}