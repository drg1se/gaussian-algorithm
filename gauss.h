//
// Created by User on 16.01.2020.
//

#ifndef GAUSS_H
#define GAUSS_H

//
// Created by User on 16.01.2020.
//



#include <vector>
#include <iostream>
#include <algorithm>
template<typename T>
using Row = std::vector<T>;

template<typename T>
using Matrix = std::vector<Row<T>>;

template<typename T>
class RowOperation
{
public:
    virtual Matrix<T>& operator () (Matrix<T>& matrix) {return matrix;}
    virtual std::ostream& show (std::ostream& os) {return os;} //debug function
    /*
     * function needed to keep indices valid during recursion:
     * called when an operation is given from a one iteration deeper step of recursion
     */
    virtual RowOperation& deeper() { return *this; };
};

template<typename T>
class t : public RowOperation<T> //transposing operation
{
private:
    int r, s;
public:
    t (int r, int s) : r(r), s(s) {};

    Matrix<T>& operator () (Matrix<T>& matrix) override
    {
        matrix[r].swap(matrix[s]);
        
        return matrix;
    }

    std::ostream& show(std::ostream& os) override
    {
        os << "t(" << r <<',' << s << ")\n";
        return os;
    }

    t& deeper () override
    {
        ++r;
        ++s;
        return *this;
    }
};

template<typename T>
class d : public RowOperation<T> // multiplying row by scalar operation
{
private:
    int r;
    T lambda;
public:
    d(int r, const T& lambda) : r(r), lambda(lambda) {}

    Matrix<T>& operator () (Matrix<T>& matrix) override
    {
        for (T& elem : matrix[r])
            elem *= lambda;

        return matrix;
    }

    std::ostream& show(std::ostream& os) override
    {
        os << "d(" << r <<',' << lambda << ")\n";
        return os;
    }

    d& deeper() override
    {
        ++r;
        return *this;
    }
};

template<typename T>
class l : public RowOperation<T> // adding one row multiplied by a scalar to another operation
{
private:
    int r, s;
    T lambda;
public:
    l(int r, int s, T lambda) : r(r), s(s), lambda(lambda) {}

    Matrix<T>& operator () (Matrix<T>& matrix) override
    {
        for (size_t i = 0; i < matrix[r].size(); ++i) {
            matrix[r][i] += matrix[s][i] * lambda;
        }

        return matrix;
    }

    std::ostream& show(std::ostream& os) override
    {
        os << "l(" << r <<',' << s << ',' << lambda << ")\n";
        return os;
    }

    l& deeper () override
    {
        ++r;
        ++s;
        return *this;
    }
};

template <typename T>
std::ostream& operator << (std::ostream& os, const Matrix<T>& matrix)
{
    for (Row<T> row : matrix)
    {
        for (T elem : row)
            os << elem << ' ';
        os << '\n';
    }
    return os;
}

template <typename T>
bool is_zero(const Row<T>& row)
{
    for (T elem : row)
        if (elem != T(0))
            return false;

    return true;
}

template <typename T>
std::vector<size_t> get_leading(const Matrix<T>& matrix) // get indices of rightest non-zero element
{
    for (size_t j = 0; j < matrix[0].size(); ++j)
        for (size_t i = 0; i < matrix.size(); ++i)
            if (matrix[i][j])
            {
                return {i, j};
            }

    return {};
}

template <typename T>
std::vector<RowOperation<T>*> REF_ops(Matrix<T> matrix) // returns all operations needed to put a matrix in REF form
{
    std::vector<RowOperation<T>*> out;

    int n = matrix.size() - 1;
    for (int i = 0; n - i > 0; ++i)
        if (is_zero(matrix[i])) {
            t<T>(i, n)(matrix);
            out.push_back(new t<T>(i, n));
            --n;
            --i;
        }



    if (n > -1)
        if(!is_zero(matrix[0]))
        {
            std::vector<size_t> lead = get_leading(matrix);

            if (lead[0]) {
                t<T>(0, lead[0])(matrix);
                out.push_back(new t<T>(0, lead[0]));
            }
            out.push_back(new d<T>(0, 1/matrix[0][lead[1]]));
            (*out.back())(matrix);
            // turn all elements of the top leading element's column bellow into zero (except the top leading one)
            for (size_t i = 1; i < matrix.size(); ++i) {
                out.push_back(new l<T>(i, 0, -matrix[i][lead[1]]));
                (*out.back())(matrix);
                // resizing the matrix from (n, m) -> (n, m - 1)
                matrix[i].erase(matrix[i].begin(), matrix[i].begin() + lead[1] + 1);
            }
            // resizing the matrix from (n, m - 1) -> (n - 1, m - 1)
            matrix.erase(matrix.begin());
            std::vector<RowOperation<T>*> smaller = REF_ops(matrix);
            std::for_each(smaller.begin(), smaller.end(), [&out](RowOperation<T>* op) {
                op->deeper();
                out.push_back(op);
            });
            return out;
        }

    return {};

}

template <typename T>
std::vector<RowOperation<T>*> REF_to_RREF_ops (Matrix<T> matrix)
{
    std::vector<RowOperation<T>*> out;
    for(size_t i  = 0; i < matrix.size(); ++i)
        for (size_t j = 0; j < matrix[0].size(); ++j)
            if (matrix[i][j])
            {
                for (size_t k = 0; k < i; ++k) {
                    out.push_back(new l<T>(k, i, -matrix[k][j]));
                    (*out.back())(matrix);
                }
                break;
            }
    return out;
}

template <typename T>
std::vector<RowOperation<T>*> RREF_ops (Matrix<T> matrix)
{
    std::vector<RowOperation<T>*> out(REF_ops(matrix)), ref_to_rref_ops;

    for (RowOperation<T>* op : out)
        (*op)(matrix);

    ref_to_rref_ops = REF_to_RREF_ops(matrix);
    out.insert(out.end(), ref_to_rref_ops.begin(), ref_to_rref_ops.end());

    return out;
}


#endif //GAUSS_H
