//
// Created by User on 17.01.2020.
//

#ifndef RATIONAL_H
#define RATIONAL_H

#include <iostream>
#include <string>
#include <sstream>

/*
 * Prototypes
 * and
 * Declarations
 */

class Rational
{
public:
    Rational (int num = 0, int denom = 1);

    int numerator () const;
    int denominator () const;
    Rational operator+ (const Rational& b) const;
    Rational operator- (const Rational& b) const;
    Rational operator+ () const;
    Rational operator- () const;
    Rational operator* (const Rational& b) const;
    Rational operator/ (const Rational& b) const;
    template <class T>
    Rational operator += (const T& b);
    template <class T>
    Rational& operator -= (const T& b);
    template <class T>
    Rational& operator *= (const T& b);
    template <class T>
    Rational& operator /= (const T& b);
    Rational& operator++ ();
    Rational& operator-- ();
    Rational operator++ (int);
    Rational operator-- (int);
    inline bool operator== (const Rational& b) const;
    inline bool operator!= (const Rational& b) const;

    operator bool () const;

private:
    int num_, denom_;
};

Rational operator+ (const Rational& a, int b);
Rational operator- (const Rational& a, int b);
Rational operator* (const Rational& a, int b);
Rational operator/ (const Rational& a, int b);
Rational operator+ (int a, const Rational& b);
Rational operator- (int a, const Rational& b);
Rational operator* (int a, const Rational& b);
Rational operator/ (int a, const Rational& b);

/*
 * Implementations
 */

int gcd(int A, int B)
{
    int a = A, b = B, tmp;
    while (a)
    {
        tmp = a;
        a = b % a;
        b = tmp;
    }
    return b;
}

Rational::Rational(int num, int denom)
{
    if (denom == 0)
        throw "Division by zero!";
    int GCD = gcd(num, denom);
    num_ = num / GCD;
    denom_ = denom / GCD;
    if (denom_ < 0)
    {
        num_ *= -1;
        denom_ *= -1;
    }
}

int Rational::numerator() const { return num_; }

int Rational::denominator() const { return denom_; }

Rational Rational::operator+(const Rational &b) const
{
    return {num_ * b.denominator() + b.numerator() * denom_, denom_ * b.denominator()};
}

Rational Rational::operator-() const { return {-num_, denom_}; }

Rational Rational::operator-(const Rational &b) const { return *this + (-b); }

Rational Rational::operator+() const { return *this; }

Rational Rational::operator*(const Rational &b) const
{
    return {num_ * b.numerator(), denom_ * b.denominator()};
}

Rational Rational::operator/(const Rational &b) const
{
    return {num_ * b.denominator(), denom_ * b.numerator()};
}

Rational operator+ (const Rational& a, int b)
{
    return {a.numerator() + b * a.denominator(), a.denominator()};
}
Rational operator- (const Rational& a, int b) { return a + (-b); }
Rational operator* (const Rational& a, int b)
{
    return {a.numerator() * b, a.denominator()};
}

Rational operator/ (const Rational& a, int b)
{
    return {a.numerator(), a.denominator() * b};
}

Rational operator+ (int a, const Rational& b) { return b + a; }
Rational operator- (int a, const Rational& b) { return a + (-b); }
Rational operator* (int a, const Rational& b) { return b * a; };
Rational operator/ (int a, const Rational& b)
{
    return {a * b.denominator(), b.numerator()};
}

template <class T>
Rational Rational::operator+=(const T &b)
{
    *this = *this + b;
    return *this;
}

template <class T>
Rational& Rational::operator-=(const T &b)
{
    *this = *this - b;
    return *this;
}

template <class T>
Rational& Rational::operator*=(const T &b)
{
    *this = *this * b;
    return *this;
}

template <class T>
Rational& Rational::operator/=(const T &b)
{
    *this = *this / b;
    return *this;
}

Rational& Rational::operator++()
{
    *this += 1;
    return *this;
}

Rational& Rational::operator--()
{
    *this -= 1;
    return *this;
}

Rational Rational::operator++(int)
{
    int num = num_, denom = denom_;
    *this += 1;
    return {num, denom};
}

Rational Rational::operator--(int)
{
    int num = num_, denom = denom_;
    *this -= 1;
    return {num, denom};
}

inline bool Rational::operator==(const Rational &b) const
{
    return num_ == b.numerator() && denom_ == b.denominator();
}

inline bool Rational::operator!=(const Rational &b) const { return !(*this == b); }

Rational::operator bool () const { return (num_ != 0); }

std::ostream& operator << (std::ostream& os, const Rational& frac)
{
    if (frac.numerator())
        if (frac.denominator() == 1)
            return os << frac.numerator();
        else
            return os << frac.numerator() << '/' << frac.denominator();
    else
        return os << '0';
}

std::istream& operator >> (std::istream& is, Rational& frac)
{
    std::string input;
    is >> input;
    if (input.find('/') == input.npos){
        int num = std::stoi(input);
        frac = Rational(num);
    } else {
        std::stringstream ss(input);
        std::string buf;
        std::getline(ss, buf, '/');
        int num = std::stoi(input);
        ss >> input;
        int denom = std::stoi(input);
        frac = Rational(num, denom);
    }
    return is;
}

#endif //RATIONAL_H
