## Quick start
### For whom
CS/Math/Physics/etc. students that use LaTeX(not necessary) for their home assignments
in Linear Algebra.
### What is it for
Takes matrix in LaTeX/"human" form and prints its RREF/Inverse in the form 
it was entered.
### What is required
_C++ compiler_ (11+ is recommended), *python3* (3.7+ is recommended)


### How to use [for humans] 
1. Compile the *CMake* project (check if the **.exe** files of the project are in
cmake-build-debug/release folder)
2. If you need to run it in "human" form with all the entries
    entered as integers or fractions (of form *num/denom*)
    to find the Reduced Row Echelon Form(RREF)
    run the following command in the project directory:
    ```
   py wrap.py rref frac n m 
   ``` 
   where `n` and `m` are the parameters of your matrix. In case you
   wish to work with decimals remove the `frac` parameter. \
   To find the inverse:
    ```
    py wrap.py inverse frac n m 
    ```
   if you put neither `rref` nor `inverse` after `wrap.py` then all of the following
   words are interpreted as flags (except for the last two which are reserved
   for matrix dimensions) and RREF of the input matrix will be printed.  
## Detailed step-by-step solution
In case you need detailed solution simply put the `detailed` flag, i.e.
the following command 
```
py wrap.py rref detailed frac 3 6 
```
followed by this input:
```$xslt
-1/2 1/2 -1/2 1 0 0
-1 0 -2 0 1 0
3/2 1/2 5/2 0 0 1
```
would result into the following output:
```$xslt

         -1/2 1/2 -1/2 1 0 0
         -1 0 -2 0 1 0
         3/2 1/2 5/2 0 0 1

d(0,-2)

         1 -1 1 -2 0 0
         -1 0 -2 0 1 0
         3/2 1/2 5/2 0 0 1

l(1,0,1)

         1 -1 1 -2 0 0
         0 -1 -1 -2 1 0
         3/2 1/2 5/2 0 0 1

l(2,0,-3/2)

         1 -1 1 -2 0 0
         0 -1 -1 -2 1 0
         0 2 1 3 0 1

d(1,-1)

         1 -1 1 -2 0 0
         0 1 1 2 -1 0
         0 2 1 3 0 1

l(2,1,-2)

         1 -1 1 -2 0 0
         0 1 1 2 -1 0
         0 0 -1 -1 2 1

d(2,-1)

         1 -1 1 -2 0 0
         0 1 1 2 -1 0
         0 0 1 1 -2 -1

l(0,1,1)

         1 0 2 0 -1 0
         0 1 1 2 -1 0
         0 0 1 1 -2 -1

l(0,2,-2)

         1 0 0 -2 3 2
         0 1 1 2 -1 0
         0 0 1 1 -2 -1

l(1,2,-1)

         1 0 0 -2 3 2
         0 1 0 1 1 1
         0 0 1 1 -2 -1
```
where 
* _l(r, s, a)_ &ndash; the operation of addition of _s_-th row 
   multiplied by scalar _a_ to the _r_-th row (*row[r] +=row[s]* * *a*)
* _d(r, a)_ &ndash; the operation of multiplying the _r_-th row
    by scalar _a_ (*row[r]* *=  *a*)
* _t(r, s)_ &ndash; the operation of swapping rows _r_ and _s_
## LaTeX part

---
In order to work with LaTeX markup use flag `latex` and to keep
the environment of your matrix use flag `env` (`detailed` flag 
is also available in LaTeX mode [_mathtools_ package is required]):
```$xslt
py wrap.py inverse latex env frac 3 3
``` 
```$xslt
  \begin{bmatrix}
    -\frac{1}{2} & \frac{1}{2} & -\frac{1}{2} \\
    -1 & 0 & -2 \\
    \frac{3}{2} & \frac{1}{2} & \frac{5}{2}
  \end{bmatrix}
```
```$xslt
\begin{bmatrix}
         -2 & 3 & 2 \\
         1 & 1 & 1 \\
         1 & -2 & -1 \\
  \end{bmatrix}
```
&ndash; the `\frac` environment is processed correctly. 