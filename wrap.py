from subprocess import Popen, PIPE
from os import walk, getcwd
from sys import argv
from re import sub

trail_space_msg_err = 'Inappropriate input: either wrong number of rows is provided or header/footer of LaTeX ' \
                  'environment (such as array or matrix) is missing'
trail_space_msg_nparsed = 'was not parsed; possibly there is some problem with trailing '\
                                                  'spaces: they are forbidden'


def in_args(*words):
    return any([arg.lower() in words for arg in argv])


def print_latex(*args, **kwargs):
    out = []

    if kwargs.get('op'):
        out = ['\\xrightarrow{']

    for arg in args:
        out.append(sub(r'(-?\d+)/(-?\d+)', r"\\frac{\1}{\2} ", str(arg)).replace('(', '_{').replace(')', '}'))
        if not kwargs.get('op') and arg != '\t':
            out.append('&')

    if kwargs.get('op'):
        out.append('}')
    else:
        out[-1] = '\\\\'

    print(*out)


def parse_latex(n, inp, frac=True, env=False):
    res = {}
    indicies = range(n)

    if env:
        res['header'] = inp[0]
        res['footer'] = inp[-1]
        indicies = range(1, n+1)
    else:
        res['header'] = ''
        res['footer'] = ''

    if frac:
        res['input'] = []
        for i in indicies:
            # TODO: add support for markup with trailing whitespaces
            res['input'].append(inp[i].replace('\\frac{', '').replace('}{', '/').replace('}', ''))

    else:
        res['input'] = inp[indicies[0]:indicies[-1] + 1]

    return res


# basically useless but left for further purposes
def parse_human(frac, inp):
    res = []
    for row in inp:
        if frac:
            for word in row.split():
                if '/' in word:
                    clean = word.split('/')
                    try:
                        res.append(tuple(map(int, clean)))
                    except:
                        print(word, 'on line', i, trail_space_msg_nparsed)
                else:
                    res.append(int(word))

        else:
            res.append(list(map(int, row.split())))

    return res


def main(dir):
    latex = in_args('latex')
    env = in_args('env') and latex
    detailed = in_args('detailed')
    decimal = in_args('decimal', 'dec')
    fractions = in_args('fractions', 'frac') and not decimal
    n, m = int(argv[-2]), int(argv[-1])
    inp = [input().replace('\t', '').replace('\\\\', '').replace(' & ', ' ') for i in range(n + 2*int(env))]
    header = None
    footer = None

    if latex:
        parsed = parse_latex(n, inp, fractions, env)
        header = parsed['header']
        footer = parsed['footer']
        inp = parsed['input']

    else:
        header = ''
        footer = ''

    ios = Popen(
        [''.join(['.\\cmake-build-', dir, '\\', 'Inverse' if argv[1].lower() == 'inverse' else 'RREF', '.exe'])],
        shell=True,
        stdout=PIPE,
        stdin=PIPE
    )

    ios.stdin.write(bytes(' '.join(map(lambda b: str(int(b)), [detailed, fractions, n, m])) + '\n', 'UTF-8'))
    ios.stdin.flush()

    for row in inp:
        ios.stdin.write(bytes(row + '\n', 'UTF-8'))

    printt = print

    if latex and fractions:
        printt = print_latex

    ios.stdin.flush()
    c = 0  # counter
    for line in ios.stdout.readlines():
        if c == 0 or c == n + 1:
            c += 1
            continue
        if c <= n:
            if c == 1:
                print(header)
            printt('\t', *line[:-2].decode('utf-8').split())
            if c == n:
                print(footer)
            c += 1
        else:
            c = 0
            if latex:
                printt(*line[:-2].decode('utf-8').split(), op=True)
            else:
                print(line[:-2].decode('utf-8'))








dirs = [d[0] for d in walk('.')]
if '.\\cmake-build-release' in dirs:
    main('release')
elif '.\\cmake-build-debug' in dirs:
    main('debug')
else:
    print("Seems like you have not compiled the CMake Project!")

